﻿using Microsoft.EntityFrameworkCore;
using RushHour.DataAccess.Model;
using System;

namespace RushHour.DataAccess
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
        : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<AppointmentActivity> AppointmentActivities { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            

            modelBuilder.Entity<User>()
                .HasMany(u => u.Roles)
                .WithMany(r => r.Users)
                .UsingEntity<UserRole>(
                    j => j
                        .HasOne(ur => ur.Role)
                        .WithMany(r => r.UserRoles)
                        .HasForeignKey(ur => ur.RoleId),
                    j => j
                        .HasOne(ur => ur.User)
                        .WithMany(u => u.UserRoles)
                        .HasForeignKey(ur => ur.UserId),
                    j =>
                    {
                        j.HasKey(t => new { t.UserId, t.RoleId });
                    });

            modelBuilder.Entity<Appointment>()
                .HasMany(ap => ap.Activities)
                .WithMany(ac => ac.Appointments)
                .UsingEntity<AppointmentActivity>(
                    j => j
                        .HasOne(aa => aa.Activity)
                        .WithMany(ac => ac.AppointmentActivities)
                        .HasForeignKey(aa => aa.ActivityId),
                    j => j
                        .HasOne(aa => aa.Appointment)
                        .WithMany(ap => ap.AppointmentActivities)
                        .HasForeignKey(aa => aa.AppointmentId),
                    j =>
                    {
                        j.HasKey(t => new { t.AppointmentId, t.ActivityId });
                    });

            modelBuilder.Entity<Appointment>()
                .HasOne(ap => ap.User)
                .WithMany(u => u.Appointments)
                .HasForeignKey(ap => ap.UserId);
        }
    }

}
