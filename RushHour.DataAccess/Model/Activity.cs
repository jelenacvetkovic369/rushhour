﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace RushHour.DataAccess.Model
{
    public class Activity : BaseEntity
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public int Duration { get; set; } //minutes

        [Required]
        public decimal Price { get; set; }

        public ICollection<Appointment> Appointments { get; set; }
        public List<AppointmentActivity> AppointmentActivities { get; set; }

    }
}
