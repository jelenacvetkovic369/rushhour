﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace RushHour.DataAccess.Model
{
    public class Appointment : BaseEntity
    {
        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        public User User { get; set; }
        public Guid UserId { get; set; }

        public ICollection<Activity> Activities { get; set; }
        public List<AppointmentActivity> AppointmentActivities { get; set; }

    }
}
