﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.DataAccess.Model
{
    public class AppointmentActivity
    {
        public Guid AppointmentId { get; set; }
        public Appointment Appointment { get; set; }

        public Guid ActivityId { get; set; }
        public Activity Activity { get; set; }
    }
}
