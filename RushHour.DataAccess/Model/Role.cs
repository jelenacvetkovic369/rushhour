﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace RushHour.DataAccess.Model
{
    public class Role : BaseEntity
    {
        [Required]
        [MaxLength(6)]
        public string Name { get; set; }

        public ICollection<User> Users { get; set; }
        public List<UserRole> UserRoles { get; set; }

    }
}
