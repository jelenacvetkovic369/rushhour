﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;

namespace RushHour.DataAccess.Model
{
    public class User : BaseEntity
    {
        [Required]
        [MaxLength(35)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(35)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(254)]
        public string Email { get; set; }

        [Required]
        [MinLength(8)]
        public string Password { get; set; }

        public byte[] Salt { get; set; }

        public ICollection<Role> Roles { get; set; }

        public ICollection<Appointment> Appointments { get; set; }

        public List<UserRole> UserRoles { get; set; }
    }
}
