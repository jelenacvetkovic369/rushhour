﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.DataAccess.Model;
using RushHour.DataAccess.RepositoryInterfaces;
using RushHour.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.DataAccess.Repository
{
    public class ActivityRepository : BaseRepository<Activity, ActivityDto>, IActivityRepository
    {
        public ActivityRepository(ApplicationContext context, IMapper mapper) : base(context, mapper) { }

        public Task<bool> ActivityNameExistsAsync(string value)
        {
            return (Entities.AnyAsync(i => i.Name == value));
        }

        public async Task<ActivityDto[]> GetActivities(Guid[] ids)
        {
            
            return await Mapper.ProjectTo<ActivityDto>(Entities.AsNoTracking().Where(l => ids.Contains(l.Id))).ToArrayAsync();
        }

        public Task<List<ActivityDto>> GetAppointmentsActivitiesAsync(Guid appointmentId)
        {
            var activities = Context.AppointmentActivities.Where(a => a.AppointmentId == appointmentId).Select(a => a.Activity);
            return Mapper.ProjectTo<ActivityDto>(activities).ToListAsync();
        }
        public Task<List<ActivityDto>> GetFilteredActivitiesAsync(int page, int size, int? fromMinutes, int? toMinutes, decimal? fromPrice, decimal? toPrice)
        {
            IQueryable<Activity> activities=Entities;
            if (fromMinutes!=null)
                activities = activities.Where(a => a.Duration>=fromMinutes);
            if(toMinutes!=null)
                activities = activities.Where(a => a.Duration <= toMinutes);
            if(fromPrice!=null)
                activities = activities.Where(a => a.Price>= fromPrice);
            if (toPrice != null)
                activities = activities.Where(a => a.Price <= toPrice);

            activities = activities.Skip((page-1) * size).Take(size);
            return Mapper.ProjectTo<ActivityDto>(activities).ToListAsync();
        }
    }
}
