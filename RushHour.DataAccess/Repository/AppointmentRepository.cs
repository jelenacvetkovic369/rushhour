﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.DataAccess.Model;
using RushHour.DataAccess.RepositoryInterfaces;
using RushHour.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.DataAccess.Repository
{
    public class AppointmentRepository : BaseRepository<Appointment, AppointmentDto>, IAppointmentRepository
    {
        public AppointmentRepository(ApplicationContext context, IMapper mapper) : base(context, mapper) { }

        public async override Task<AppointmentDto> GetByIdAsync(Guid id)
        {
            var appointment = await Entities.AsNoTracking().Include(u => u.Activities).FirstOrDefaultAsync(i => i.Id == id);
            return Mapper.Map<AppointmentDto>(appointment);
        }

        public Task RemoveActivity(Guid appointmentId, Guid activityId)
        {
            AppointmentActivity aa=Context.AppointmentActivities.Find(appointmentId, activityId);
            Context.AppointmentActivities.Remove(aa);
            return Context.SaveChangesAsync();
        }

        public Task AddActivity(Guid appointmentId, Guid activityId)
        {
            Context.AppointmentActivities.AddAsync(new AppointmentActivity() { AppointmentId = appointmentId, ActivityId = activityId });
            return Context.SaveChangesAsync();
        }

        public Task<bool> AppointmentActivityExists(Guid appointmentId, Guid activityId)
        {
            return Context.AppointmentActivities.AnyAsync(aa => aa.AppointmentId == appointmentId && aa.ActivityId == activityId);
        }

        public Task<Guid[]> GetAppointmentIfOverlapping(DateTime startDate, DateTime endDate)
        {
            return Entities.Where(l => startDate < l.EndDate && l.StartDate < endDate).Select(a => a.Id).ToArrayAsync();
        }

        public Task AddActivities(Guid appointmentId, Guid[] activityIds)
        {
            foreach (Guid activityId in activityIds)
            {
                Context.AppointmentActivities.AddAsync(new AppointmentActivity() { AppointmentId = appointmentId, ActivityId = activityId });
            }
            return Context.SaveChangesAsync();
        }

        public Task<List<AppointmentDto>> GetUsersAppointmentsAsync(Guid userId)
        {
            var appointments = Entities.Where(a => a.UserId == userId);
            return Mapper.ProjectTo<AppointmentDto>(appointments).ToListAsync();
        }

        public Task<List<AppointmentDto>> GetFilteredAppointmentsAsync(int page, int size, DateTime? fromDate, DateTime? toDate)
        {
            IQueryable<Appointment> appointments = Entities;
            if(fromDate!=null)
                appointments= appointments.Where(a => a.StartDate>=fromDate);
            if (toDate != null)
                appointments = appointments.Where(a => a.StartDate <= toDate);

            appointments= appointments.Skip((page - 1) * size).Take(size);
            return Mapper.ProjectTo<AppointmentDto>(appointments).ToListAsync();
        }

        public Task<bool> CheckAccess(Guid appointmentId, Guid userId)
        {
            return Entities.Where(a => a.Id == appointmentId && a.UserId == userId).AnyAsync();
        }
    }
}
