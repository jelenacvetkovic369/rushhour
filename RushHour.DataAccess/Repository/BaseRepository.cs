﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.DataAccess.Model;
using RushHour.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.DataAccess.Repository
{
    public class BaseRepository<T, TShared> : IBaseRepository<T, TShared> where T : BaseEntity 
        where TShared : BaseDto
    {
        protected ApplicationContext Context { get; set; }
        protected DbSet<T> Entities { get; set; }
        protected IMapper Mapper { get; set; }
        public BaseRepository(ApplicationContext context, IMapper mapper)
        {
           
            this.Context = context;
            Entities = context.Set<T>();
            this.Mapper = mapper;
        }


        public async Task<Guid> AddAsync(TShared entityShared)
        {
            T entity = Mapper.Map<T>(entityShared);

            Entities.Add(entity);
            await Context.SaveChangesAsync();
            return entity.Id;
        }

        public virtual async Task<TShared> GetByIdAsync(Guid id)
        {           
            var entity= await Entities.AsNoTracking().FirstOrDefaultAsync(i => i.Id == id);
            TShared entityShared = Mapper.Map<TShared>(entity);
            return entityShared;
        }

        public virtual Task<TShared> GetByPropertyAsync(string value)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(TShared entityShared)
        {
            T entity = Mapper.Map<T>(entityShared);

            Entities.Update(entity);
            return Context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            T entity = await Entities.FindAsync(id);
            if (entity != null)
            {
                Entities.Remove(entity);
                await Context.SaveChangesAsync();
            }
        }

        public  Task<List<TShared>> GetEntities(int size)
        {
            var entities = Entities.Take(size);
            return Mapper.ProjectTo<TShared>(entities).ToListAsync();
        }

        public Task<bool> EntityExists(Guid id)
        {
            return Entities.AnyAsync(e => e.Id == id);
        }
    }

}
