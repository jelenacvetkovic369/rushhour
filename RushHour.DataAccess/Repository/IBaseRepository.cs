﻿
using RushHour.DataAccess.Model;
using RushHour.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.DataAccess.Repository
{
    public interface IBaseRepository<T, TShared> where T : BaseEntity 
        where TShared : BaseDto
    {
        Task<Guid> AddAsync(TShared entity);
        Task<TShared> GetByIdAsync(Guid id);
        Task<TShared> GetByPropertyAsync(string value);
        Task UpdateAsync(TShared entity);
        Task DeleteAsync(Guid id);
        Task<List<TShared>> GetEntities(int size);
        Task<bool> EntityExists(Guid id);
    }

}
