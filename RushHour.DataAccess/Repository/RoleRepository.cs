﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.DataAccess.Model;
using RushHour.Shared.Dto;
using System.Threading.Tasks;

namespace RushHour.DataAccess.Repository
{
    public class RoleRepository : BaseRepository<Role, RoleDto>
    {
        public RoleRepository(ApplicationContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<RoleDto> FindRole(string name)
        {
            var role = await Entities.AsNoTracking().FirstOrDefaultAsync(i => i.Name == name);
            RoleDto roleShared= Mapper.Map<RoleDto>(role);
            return roleShared;
        }

       
    }
}
