﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.DataAccess.Model;
using RushHour.DataAccess.RepositoryInterfaces;
using RushHour.Shared.Dto;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.DataAccess.Repository
{
    public class UserRepository : BaseRepository<User, UserDto>, IUserRepository
    {
        public UserRepository(ApplicationContext context, IMapper mapper) : base(context, mapper) { }

        public async override Task<UserDto> GetByPropertyAsync(string value)
        {
            var user=await Entities.AsNoTracking().Include(u => u.Roles).FirstOrDefaultAsync(i => i.Email == value);
            return Mapper.Map<UserDto>(user);
        }


        public Task<bool> EmailExistsAsync(string email)
        {
            return Entities.AnyAsync(i => i.Email == email); 
        }

        public Task AddRoleForUser(Guid userId, Guid roleId)
        {
            Context.UserRoles.Add(new UserRole { UserId = userId, RoleId = roleId });
            return Context.SaveChangesAsync();
        }

        public Task<bool> HasRoleAsync(Guid userId, string roleName)
        {
            return Entities.AnyAsync(user => user.Id == userId && user.Roles.Any(role=>role.Name==roleName));
        }
    }
}
