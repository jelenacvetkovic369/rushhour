﻿using RushHour.DataAccess.Repository;
using RushHour.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.DataAccess.RepositoryInterfaces
{
    public interface IActivityRepository 
    {
        Task<Guid> AddAsync(ActivityDto entity);
        Task<bool> ActivityNameExistsAsync(string value);
        Task<ActivityDto[]> GetActivities(Guid[] ids);
        Task<List<ActivityDto>> GetAppointmentsActivitiesAsync(Guid appointmentId);
        Task<List<ActivityDto>> GetFilteredActivitiesAsync(int page, int size, int? fromMinutes, int? toMinutes, decimal? fromPrice, decimal? toPrice);
    }
}
