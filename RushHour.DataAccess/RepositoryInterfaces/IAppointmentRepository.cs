﻿using RushHour.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.DataAccess.RepositoryInterfaces
{
    public interface IAppointmentRepository
    {
        Task<AppointmentDto> GetByIdAsync(Guid id);
        Task RemoveActivity(Guid appointmentId, Guid activityId);
        Task AddActivity(Guid appointmentId, Guid activityId);
        Task<bool> AppointmentActivityExists(Guid appointmentId, Guid activityId);
        Task<Guid[]> GetAppointmentIfOverlapping(DateTime startDate, DateTime endDate);
        Task AddActivities(Guid appointmentId, Guid[] activityIds);
        Task<List<AppointmentDto>> GetUsersAppointmentsAsync(Guid userId);
        Task<List<AppointmentDto>> GetFilteredAppointmentsAsync(int page, int size, DateTime? fromDate, DateTime? toDate);
        Task<bool> CheckAccess(Guid appointmentId, Guid userId);
    }
}
