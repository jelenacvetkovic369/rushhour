﻿using RushHour.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.DataAccess.RepositoryInterfaces
{
    public interface IUserRepository
    {
        Task<UserDto> GetByPropertyAsync(string value);
        Task<bool> EmailExistsAsync(string email);
        Task AddRoleForUser(Guid userId, Guid roleId);
        Task<bool> HasRoleAsync(Guid userId, string roleName);
    }
}
