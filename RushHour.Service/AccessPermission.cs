﻿
namespace RushHour.Service
{
    public static class AccessPermission
    {
        public const string UserAndAdmin = "UserAndAdmin";
        public const string Admin = "Admin";
        public const string User = "User";
    }
}
