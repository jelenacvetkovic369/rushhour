﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.Service.DataModel
{
    public class ActivityInsert
    {
        public string Name { get; set; }
        public int Duration { get; set; }
        public decimal Price { get; set; }
    }
}
