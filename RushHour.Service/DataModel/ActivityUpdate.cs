﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.Service.DataModel
{
    public class ActivityUpdate
    {
        public int? Duration { get; set; }
        public decimal? Price { get; set; }
    }
}
