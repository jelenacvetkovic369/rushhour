﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.Service.DataModel
{
    public class AppointmentInsert
    {
        public DateTime StartDate { get; set; }
        public Guid UserId { get; set; }
        public Guid[] ActivityIds { get; set; }
    }
}
