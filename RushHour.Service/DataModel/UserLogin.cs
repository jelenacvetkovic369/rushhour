﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.Service.DataModel
{
    public class UserLogin
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
