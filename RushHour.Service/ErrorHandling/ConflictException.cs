﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.Service.ErrorHandling
{
    public class ConflictException : HttpStatusException
    {
        public ConflictException(string msg) : base(msg) { }
    }
}
