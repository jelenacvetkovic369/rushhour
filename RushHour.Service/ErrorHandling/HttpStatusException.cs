﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace RushHour.Service.ErrorHandling
{
    public class HttpStatusException : Exception
    {
        public string message;
        public HttpStatusException(string msg) : base(msg)
        {

        }
    }
}
