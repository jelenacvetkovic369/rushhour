﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.Service.ErrorHandling
{
    public class NotFoundException : HttpStatusException
    {
        public NotFoundException(string msg) : base(msg) { }
    }
}
