﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.Service.ErrorHandling
{
    public class UnauthorizedException : HttpStatusException
    {
        public UnauthorizedException(string msg) : base(msg) { }
    }
}
