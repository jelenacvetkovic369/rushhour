﻿

namespace RushHour.Service.ErrorHandling
{
    public class ValidationException : HttpStatusException
    {
        public ValidationException(string msg) : base(msg) { }
    }
}
