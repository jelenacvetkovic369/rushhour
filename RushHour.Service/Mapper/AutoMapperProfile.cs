﻿using AutoMapper;
using RushHour.DataAccess.Model;
using RushHour.Service.DataModel;
using RushHour.Shared.Dto;
using System.Linq;

namespace RushHour.Service.Mapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            ConfigureMappings();
        }
        private void ConfigureMappings()
        {
            CreateMap<UserDto, User>() 
                .ReverseMap()
                .ForMember(d => d.RoleNames, o => o.MapFrom(s => s.Roles.Select(c => c.Name).ToArray())); 
            CreateMap<Role, RoleDto>().ReverseMap();
            CreateMap<ActivityDto, Activity>().ForMember(d=>d.Duration, opt => opt.MapFrom(s => s.DurationInMinutes));
            CreateMap<Activity, ActivityDto>().ForMember(d => d.DurationInMinutes, opt => opt.MapFrom(s => s.Duration));
            CreateMap<AppointmentDto, Appointment>()
                .ReverseMap()
                .ForMember(d => d.TotalDuration, o => o.MapFrom(s => s.Activities.Sum(c => c.Duration))); 
            CreateMap<ActivityInsert, ActivityDto>().ReverseMap();
            CreateMap<AppointmentInsert, AppointmentDto>().ReverseMap();

            CreateMap<ActivityUpdate, ActivityDto>()
                .ForMember(d => d.DurationInMinutes, opt => { opt.MapFrom(s => s.Duration); opt.PreCondition((rc) => rc.Duration != 0); })
                .ForMember(d => d.Price, o => o.PreCondition((rc) => rc.Price == 0));

            CreateMap<UserSignUp, UserDto>();
        }
    }
}
