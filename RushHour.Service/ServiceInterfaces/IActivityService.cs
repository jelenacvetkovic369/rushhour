﻿using RushHour.Service.DataModel;
using RushHour.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Service.ServiceInterfaces
{
    public interface IActivityService
    {
        Task<Guid> AddAsync(ActivityInsert activityInsert);
        Task UpdateAsync(Guid id, ActivityUpdate activityUpdate);
        Task<List<ActivityDto>> GetFilteredActivitiesAsync(int page, int size, int? fromMinutes, int? toMinutes, decimal? fromPrice, decimal? toPrice);
        Task<ActivityDto> GetByIdAsync(Guid id);
    }
}
