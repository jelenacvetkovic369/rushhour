﻿using RushHour.Service.DataModel;
using RushHour.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Service.ServiceInterfaces
{
    public interface IAppointmentService
    {
        bool CheckAccess(Guid userId);
        Task<Guid> AddAsync(AppointmentInsert appointmentInsert);
        Task DeleteAsync(Guid id);
        Task RemoveActivityAsync(Guid appointmentId, Guid activityId);
        Task AddActivityAsync(Guid appointmentId, Guid activityId);
        Task UpdateAsync(Guid id, DateTime newStartDate);
        DateTime GetEndDate(int duration, DateTime startDate);
        Task<List<AppointmentDto>> GetUsersAppointmentsAsync(Guid userId);
        Task<List<AppointmentDto>> GetFilteredAppointmentsAsync(int page, int size, DateTime? fromDate, DateTime? toDate);
        Task<List<ActivityDto>> GetAppointmentsActivitiesAsync(Guid appointmentId);

    }
}
