﻿using RushHour.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Service.ServiceInterfaces
{
    public interface IRoleService
    {
        Task<RoleDto> GetByIdAsync(Guid id);
        Task<RoleDto> FindRole(string name);
    }
}
