﻿using RushHour.Service.DataModel;
using RushHour.Shared.Dto;
using System;
using System.Threading.Tasks;

namespace RushHour.Service.ServiceInterfaces
{
    interface IUserService
    {
        Task<Guid> AddAsync(UserSignUp user);
        bool IsValidPassword(string password);
        bool IsValidEmail(string email);
        Task<UserDto> GetValidatedUserAsync(UserLogin userlogin);
    }
}
