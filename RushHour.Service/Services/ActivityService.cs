﻿using RushHour.DataAccess.Repository;
using RushHour.Service.DataModel;
using RushHour.Service.ErrorHandling;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RushHour.Shared.Dto;
using AutoMapper;
using RushHour.Service.Services;
using RushHour.Service.ServiceInterfaces;
using RushHour.DataAccess.RepositoryInterfaces;
using RushHour.DataAccess.Model;

namespace RushHour.Service
{
    public class ActivityService : BaseService<RushHour.DataAccess.Model.Activity, ActivityDto, ActivityRepository>, IActivityService
    {
 
        public ActivityService(ActivityRepository activityRepository, IMapper mapper) : base(activityRepository, mapper)
        {
        }

        public async Task<Guid> AddAsync(ActivityInsert activityInsert)
        {          
            if (await repository.ActivityNameExistsAsync(activityInsert.Name))
                throw new ConflictException("Activity already exists.");
            
            ActivityDto activity = Mapper.Map<ActivityDto>(activityInsert);
            return await base.InsertAsync(activity);
        }

        public async Task UpdateAsync(Guid id,ActivityUpdate activityUpdate)
        {
            ValidateUpdate(activityUpdate);

            ActivityDto activityDto =await repository.GetByIdAsync(id);
            ActivityDto activity = Mapper.Map(activityUpdate,activityDto);

            await base.UpdateAsync(activity);
        }

        public void ValidateUpdate(ActivityUpdate activity)
        {
            if (activity.Duration <= 0)
                throw new ValidationException("Duration must be greater than 0.");
            if (activity.Price <= 0)
                throw new ValidationException("Price must be greater tnam 0.");
        }

        public Task<List<ActivityDto>> GetFilteredActivitiesAsync(int page, int size, int? fromMinutes, int? toMinutes, decimal? fromPrice, decimal? toPrice)
        {
            return repository.GetFilteredActivitiesAsync(page, size, fromMinutes, toMinutes, fromPrice, toPrice);
        }

    }
}
