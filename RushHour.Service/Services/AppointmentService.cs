﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using RushHour.DataAccess.Model;
using RushHour.DataAccess.Repository;
using RushHour.Service.DataModel;
using RushHour.Service.ErrorHandling;
using RushHour.Service.ServiceInterfaces;
using RushHour.Service.Services;
using RushHour.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RushHour.Service
{
    public class AppointmentService : BaseService<Appointment, AppointmentDto, AppointmentRepository>, IAppointmentService
    {
        private UserRepository userRepository;
        private ActivityRepository activityRepository;
        private IHttpContextAccessor contextAccessor;
        public AppointmentService(AppointmentRepository appointmentRepository, UserRepository userRepository, ActivityRepository activityRepository, IMapper mapper, IHttpContextAccessor contextAccessor)
            : base(appointmentRepository, mapper)
        {         
            this.userRepository = userRepository;
            this.activityRepository = activityRepository;
            this.contextAccessor = contextAccessor;
        }
        public bool CheckAccess(Guid userId)
        {
            if (!contextAccessor.HttpContext.User.IsInRole(AccessPermission.Admin))
            {
                var u = Guid.Parse(contextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
                if (u != userId)
                    return false;
            }
            return true;
                     
        }
        public async Task<Guid> AddAsync(AppointmentInsert appointmentInsert)
        {
            if (!CheckAccess(appointmentInsert.UserId))
                throw new UnauthorizedException($"Id: {appointmentInsert.UserId} isn't current user's id. User can only create appointments with his id.");

            if (!await userRepository.EntityExists(appointmentInsert.UserId))
                throw new NotFoundException("User not found.");

            if (!await userRepository.HasRoleAsync(appointmentInsert.UserId, AccessPermission.User)) 
                throw new ValidationException($"Admin {appointmentInsert.UserId} can't have appointments.");

            //get activities
            ActivityDto[] activities = await activityRepository.GetActivities(appointmentInsert.ActivityIds);

            //check if number of activities from appointmentInsert is the same in the list, if not some activites were not found
            if (activities.Count() != appointmentInsert.ActivityIds.Count())
                throw new NotFoundException("Not all activities exist.");

            //get end date
            DateTime endDate = GetEndDate(activities.Sum(a => a.DurationInMinutes), appointmentInsert.StartDate);

            //get appointments that are overlapping
            Guid[] overlappingAppointments = await repository.GetAppointmentIfOverlapping(appointmentInsert.StartDate, endDate);

            if (overlappingAppointments.Length>0)
                throw new ConflictException("This period overlaps with appointments: " + String.Join(",", overlappingAppointments.Select(g => g.ToString()).ToArray()));

            AppointmentDto appointment = Mapper.Map<AppointmentDto>(appointmentInsert);
            appointment.UserId = appointmentInsert.UserId;
            appointment.Id = Guid.NewGuid();
            appointment.EndDate = endDate;
            Guid id=await repository.AddAsync(appointment);

            //add activities to appointment
            await repository.AddActivities(appointment.Id,appointmentInsert.ActivityIds);
            return id;
        }

        public override async Task DeleteAsync(Guid id)
        {
            Guid userId = Guid.Parse(contextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            if (!contextAccessor.HttpContext.User.IsInRole(AccessPermission.Admin) && !await repository.CheckAccess(id, userId))
                throw new UnauthorizedException($"User with id: {userId} doesn't have access to appointment with id: {id}.");


            await repository.DeleteAsync(id);
        }

        public async Task RemoveActivityAsync(Guid appointmentId, Guid activityId)
        {
            AppointmentDto appointment = await base.GetByIdAsync(appointmentId);
            if(!CheckAccess(appointment.UserId))
                throw new UnauthorizedException($"User doesn't have access to appointment with id: {appointmentId}");


            //check if relationship exists
            if (! await repository.AppointmentActivityExists(appointmentId, activityId))
                throw new NotFoundException($"Appointment {appointmentId} with activity {activityId} doesn't exist.");


            //remove relationship
            await repository.RemoveActivity(appointmentId, activityId);

            //calculate endDate and set it, then update appointment
            appointment.EndDate = appointment.EndDate = GetEndDate(appointment.TotalDuration, appointment.StartDate);
            await base.UpdateAsync(appointment);
        }

        public async Task AddActivityAsync(Guid appointmentId, Guid activityId)
        {
            AppointmentDto appointment = await base.GetByIdAsync(appointmentId);
            if (!CheckAccess(appointment.UserId))
                throw new UnauthorizedException($"User doesn't have access to appointment with id: {appointmentId}");


            ActivityDto activity = await activityRepository.GetByIdAsync(activityId);
            if (activity==null)
                throw new NotFoundException("Activity doesn't exist.");


            DateTime endDate = GetEndDate(appointment.TotalDuration+activity.DurationInMinutes, appointment.StartDate);
            Guid[] overlappingAppointments = await repository.GetAppointmentIfOverlapping(appointment.StartDate, endDate);

            if (overlappingAppointments.Length > 0)
                throw new ConflictException("This period overlaps with appointments: " + String.Join(",", overlappingAppointments.Select(g => g.ToString()).ToArray()));
           
            await repository.AddActivity(appointmentId, activityId);

            appointment.EndDate = endDate;
            await base.UpdateAsync(appointment);
        }

        public async Task UpdateAsync(Guid id, DateTime newStartDate) 
        {
            AppointmentDto appointment = await base.GetByIdAsync(id);
            if (!CheckAccess(appointment.UserId))
                throw new UnauthorizedException($"User doesn't have access to appointment with id: {id}");


            DateTime endDate = GetEndDate(appointment.TotalDuration, newStartDate);
            Guid[] overlappingAppointments = await repository.GetAppointmentIfOverlapping(appointment.StartDate, endDate);

            if (overlappingAppointments.Length > 0)
                throw new ConflictException("This period overlaps with appointments: " + String.Join(",", overlappingAppointments.Select(g => g.ToString()).ToArray()));

            appointment.StartDate = newStartDate; 
            appointment.EndDate = endDate;

            await base.UpdateAsync(appointment);
        }

        public DateTime GetEndDate(int duration, DateTime startDate)
        {
            return startDate.AddMinutes(duration);
        }

        public Task<List<AppointmentDto>> GetUsersAppointmentsAsync(Guid userId)
        {
            CheckAccess(userId);
            return repository.GetUsersAppointmentsAsync(userId);
        }

        public Task<List<AppointmentDto>> GetFilteredAppointmentsAsync(int page, int size, DateTime? fromDate, DateTime? toDate)
        {
            return repository.GetFilteredAppointmentsAsync(page, size, fromDate, toDate);
        }

        public async Task<List<ActivityDto>> GetAppointmentsActivitiesAsync(Guid appointmentId)
        {
            Guid userId = Guid.Parse(contextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            if (!contextAccessor.HttpContext.User.IsInRole(AccessPermission.Admin) && !await repository.CheckAccess(appointmentId,userId))
                throw new UnauthorizedException($"User with id: {userId} doesn't have access to appointment with id: {appointmentId}");
            return await activityRepository.GetAppointmentsActivitiesAsync(appointmentId);
        }
    }
}
