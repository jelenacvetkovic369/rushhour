﻿using AutoMapper;
using RushHour.DataAccess.Model;
using RushHour.DataAccess.Repository;
using RushHour.Service.ErrorHandling;
using RushHour.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Service.Services
{
    public class BaseService<T, TDto, TRepository> : IBaseService<T, TDto, TRepository>
        where TDto : BaseDto
        where T : BaseEntity
        where TRepository : IBaseRepository<T, TDto>
    {
        protected readonly TRepository repository;
        protected IMapper Mapper { get; set; }

        protected BaseService(TRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.Mapper = mapper;
        }


        public async Task<TDto> GetByIdAsync(Guid id)
        {
            var entityDto = await repository.GetByIdAsync(id);
            if (entityDto == null)
                throw new NotFoundException("Entity not found.");
            return entityDto;
        }

        public Task<TDto> GetByPropertyAsync(string property)
        {
            var entityDto= repository.GetByPropertyAsync(property);
            if (entityDto == null)
                throw new NotFoundException("Entity not found.");
            return entityDto;
        }

        public Task<List<TDto>> GetEntitiesAsync(int size)
        {
            return repository.GetEntities(size);
        }

        public virtual Task DeleteAsync(Guid id)
        {
            return repository.DeleteAsync(id);
        }

        public Task UpdateAsync(TDto dto)
        {
            return repository.UpdateAsync(dto);
        }

        public Task<Guid> InsertAsync(TDto dto)
        {
            return repository.AddAsync(dto);
        }
    }
}
