﻿using RushHour.DataAccess.Model;
using RushHour.DataAccess.Repository;
using RushHour.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Service.Services
{
    interface IBaseService<T, TDto, TRepository> 
        where TDto : BaseDto
        where T : BaseEntity
        where TRepository : IBaseRepository<T, TDto>
    {
        Task<TDto> GetByIdAsync(Guid id);
        Task<TDto> GetByPropertyAsync(string property);
        Task<List<TDto>> GetEntitiesAsync(int size);
        Task DeleteAsync(Guid id);
        Task UpdateAsync(TDto dto);
        Task<Guid> InsertAsync(TDto dto);
    }
}
