﻿
using AutoMapper;
using RushHour.DataAccess.Model;
using RushHour.DataAccess.Repository;
using RushHour.Service.ServiceInterfaces;
using RushHour.Service.Services;
using RushHour.Shared.Dto;
using System.Threading.Tasks;

namespace RushHour.Service
{

    public class RoleService : BaseService<Role, RoleDto, RoleRepository>, IRoleService
    {

        public RoleService(RoleRepository roleRepository, IMapper mapper) : base(roleRepository, mapper)
        {

        }

        public Task<RoleDto> FindRole(string name)
        {
            return repository.FindRole(name);
        }



    }
}
