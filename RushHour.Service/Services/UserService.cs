﻿using AutoMapper;
using RushHour.DataAccess.Model;
using RushHour.DataAccess.Repository;
using RushHour.Service.DataModel;
using RushHour.Service.ErrorHandling;
using RushHour.Service.Services;
using RushHour.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Service
{
    public class UserService : BaseService<User, UserDto, UserRepository> 
    {
        private const string userRoleName= "User";
        private PasswordHasher passwordHasher;
        private RoleService roleService;

        public UserService(UserRepository userRepository, PasswordHasher passwordhasher, RoleService roleService, IMapper mapper)
            : base(userRepository,mapper)
        {
            this.passwordHasher = passwordhasher;
            this.roleService = roleService;
        }

        public async Task<Guid> AddAsync(UserSignUp user)
        {
            if (!IsValidEmail(user.Email))
                throw new ValidationException("Email format is invalid.");

            if (await repository.EmailExistsAsync(user.Email))
                throw new ConflictException("Email is not unique.");

            if(!IsValidPassword(user.Password))
                throw new ValidationException("Password did not meet requirements.");

            byte[] salt = passwordHasher.GenerateSalt();

            UserDto u = Mapper.Map<UserDto>(user);
            u.Id = Guid.NewGuid();
            u.Password = passwordHasher.HashPassword(user.Password, salt);
            u.Salt = salt;

            RoleDto userRole = await roleService.FindRole(userRoleName);

            await base.InsertAsync(u);
            await repository.AddRoleForUser(u.Id, userRole.Id);
            return u.Id;
        }

        public bool IsValidPassword(string password)
        {
            return
               password.Length > 7 &&
               password.Any(char.IsUpper) &&
               password.Any(char.IsDigit);
        }

        public bool IsValidEmail(string email)
        {
            return (email.Contains("@"));
     
        }

        public async Task<UserDto> GetValidatedUserAsync(UserLogin userlogin)
        {
            var user = await base.GetByPropertyAsync(userlogin.Email);

            if (passwordHasher.VerifyPassword(userlogin.Password, user.Password, user.Salt))
            {
                return user;
            }
            else
                throw new UnauthorizedException("Wrong password.");

        }
    }
}
