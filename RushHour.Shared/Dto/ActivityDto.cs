﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.Shared.Dto
{
    public class ActivityDto : BaseDto
    {
        public string Name { get; set; }

        public int DurationInMinutes { get; set; } 

        public decimal Price { get; set; }

    }
}
