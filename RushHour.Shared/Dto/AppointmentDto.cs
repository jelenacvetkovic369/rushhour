﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.Shared.Dto
{
    public class AppointmentDto : BaseDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid UserId { get; set; }
        public int TotalDuration { get; set; }
    }
}
