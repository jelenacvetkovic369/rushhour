﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.Shared.Dto
{
    public class RoleDto : BaseDto
    {
        public string Name { get; set; }
    }
}
