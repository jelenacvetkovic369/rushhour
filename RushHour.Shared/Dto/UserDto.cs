﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.Shared.Dto
{
    public class UserDto : BaseDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public byte[] Salt { get; set; }

        public ICollection<string> RoleNames { get; set; }
    }
}
