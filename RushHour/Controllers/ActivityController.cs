﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using RushHour.Service;
using RushHour.Service.DataModel;
using RushHour.Services;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace RushHour.Controllers
{
    [Route("api/activities")]
    [Authorize(Policy = AccessPermission.Admin)]
    public class ActivityController : Controller
    {
        private readonly ActivityService activityService;
        private readonly ISecurityService securityService;

        public ActivityController(ActivityService activityService, ISecurityService securityService)
        {
            this.activityService = activityService;
            this.securityService = securityService;
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> InsertActivityAsync(ActivityInsert activity)
        {
            Guid actId = await activityService.AddAsync(activity);
            return CreatedAtRoute("GetActivity", new { id = actId }, activity);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult>UpdatetActivityAsync(Guid id, ActivityUpdate activity)
        {
            await activityService.UpdateAsync(id,activity);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> UpdatetActivityAsync(Guid id)
        {
            await activityService.DeleteAsync(id);
            return NoContent();
        }

        [HttpGet("{id}", Name = "GetActivity")]
        public async Task<IActionResult> GetActivityAsync(Guid id)
        {
            return Ok(await activityService.GetByIdAsync(id));
        }

        [HttpGet("activities-filtered")]
        public async Task<IActionResult> GetActivitiesInPriceRangeAsync(int page, int size, int? fromMinutes, int? toMinutes, decimal? fromPrice, decimal? toPrice)
        {
            return Ok(await activityService.GetFilteredActivitiesAsync(page, size, fromMinutes, toMinutes, fromPrice, toPrice));
        }

    }
}
