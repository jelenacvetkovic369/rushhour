﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using RushHour.Service;
using RushHour.Service.DataModel;
using RushHour.Services;
using System;
using System.Threading.Tasks;

namespace RushHour.Controllers
{
    [Route("api/appointments")]
    [Authorize(Policy = AccessPermission.UserAndAdmin)]
    public class AppointmentController : Controller
    {
        private readonly AppointmentService appointmentService;
        private readonly ISecurityService securityService;
   

        public AppointmentController(AppointmentService appointmentService, ISecurityService securityService)
        {
            this.appointmentService = appointmentService;
            this.securityService = securityService;
     
        }

        [HttpPost]
        public async Task<IActionResult> InsertAppointmentAsync(AppointmentInsert appointment)
        {
            Guid appId = await appointmentService.AddAsync(appointment);
            return CreatedAtRoute("GetAppointment", new { id = appId }, appointment);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAppointmentAsync(Guid id)
        {
            
            await appointmentService.DeleteAsync(id);
            return NoContent();
        }

        [HttpGet("{id}", Name ="GetAppointment")]
        public async Task<IActionResult> GetAppointmentAsync(Guid id)
        {
            return Ok(await appointmentService.GetByIdAsync(id));
        }

        [HttpDelete("remove-activity")]
        public async Task<IActionResult> RemoveActivityFromAppointmentAsync(Guid appointmentId, Guid activityId)
        {
            await appointmentService.RemoveActivityAsync(appointmentId, activityId);
            return NoContent();
        }

        [HttpPost("add-activity")]
        public async Task<IActionResult> AddActivityToAppointmentAsync(Guid appointmentId, Guid activityId)
        {
            await appointmentService.AddActivityAsync(appointmentId, activityId);
            return Created(new Uri(Request.GetEncodedUrl()), null);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAppointmentAsync(Guid id, DateTime newStartDate)
        {
            await appointmentService.UpdateAsync(id,newStartDate);
            return NoContent();
        }

        [HttpGet("appointments-by-user")]
        public async Task<IActionResult> GetUsersAppointmentsAsync(Guid userId)
        {
            return Ok(await appointmentService.GetUsersAppointmentsAsync(userId));
        }

        [Authorize(Policy = AccessPermission.Admin)]
        [HttpGet]
        public async Task<IActionResult> GetFilteredAppointmentsAsync(int page, int size, DateTime? fromDate, DateTime? toDate)
        {
            return Ok(await appointmentService.GetFilteredAppointmentsAsync(page, size, fromDate, toDate));
        }

        [HttpGet("appointments-activities")]
        public async Task<IActionResult> GetAppointmentsActivitiesAsync(Guid appointmentId)
        {
            return Ok(await appointmentService.GetAppointmentsActivitiesAsync(appointmentId));
        }
    }
}
