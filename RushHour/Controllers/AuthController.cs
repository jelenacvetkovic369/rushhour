﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using RushHour.Service;
using RushHour.Service.DataModel;
using RushHour.Services;
using System;
using System.Threading.Tasks;

namespace RushHour.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AuthController : ControllerBase
    {
        private readonly UserService userService;
        private readonly RoleService roleService;
        private readonly ISecurityService securityService;

        public AuthController(ISecurityService securityService, UserService userService, RoleService roleService)
        {
            this.userService = userService;
            this.roleService = roleService;
            this.securityService = securityService;
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> LoginAsync(UserLogin userlogin)
        {
            var user = await userService.GetValidatedUserAsync(userlogin);
            return Ok(securityService.GenerateJwtToken(user.Id, user.RoleNames));

        }

        [HttpPost("sign-up")]
        [AllowAnonymous]
        public async Task<IActionResult> SignUpAsync(UserSignUp user)
        {
            await userService.AddAsync(user);
            return NoContent();
        }

    }
}
