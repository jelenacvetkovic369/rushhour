﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using RushHour.Service.ErrorHandling;

namespace RushHour
{
    public class ErrorMiddleware
    {
        private readonly RequestDelegate next;

        public ErrorMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleException(context, ex);
            }
        }

        private static Task HandleException(HttpContext context, Exception ex)
        {
            HttpStatusCode code = HttpStatusCode.InternalServerError;
            string result = "";
            switch (ex)
            {
                case ValidationException: 
                    code = HttpStatusCode.BadRequest;
                    result = JsonConvert.SerializeObject(new { error = ex.Message });
                    break;
                case NotFoundException:
                    code = HttpStatusCode.NotFound;
                    break;
                case ConflictException:
                    code = HttpStatusCode.Conflict;
                    result = JsonConvert.SerializeObject(new { error = ex.Message });
                    break;
                case UnauthorizedException:
                    code = HttpStatusCode.Unauthorized;
                    break;
            }

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;

            return context.Response.WriteAsync(result);
        }
    }
}
