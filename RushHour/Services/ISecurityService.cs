﻿using RushHour.DataAccess.Model;
using RushHour.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Services
{
    public interface ISecurityService
    {
        string GenerateJwtToken(Guid id, ICollection<string> roles);
    }
}
