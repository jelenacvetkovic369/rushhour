﻿using RushHour.Infrastructure;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace RushHour.Services
{
    public class SecurityService : ISecurityService
    {
        private readonly JwtSettings settings;
        public SecurityService(JwtSettings settings)
        {
            this.settings = settings;
        }
        public string GenerateJwtToken(Guid userid, ICollection<string> roles)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(settings.Key));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub,userid.ToString()),
                new Claim(JwtRegisteredClaimNames.Aud, settings.Audience),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds().ToString())
            };

            foreach (string role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var token = new JwtSecurityToken(
                issuer: settings.Issuer,
                audience: null,
                claims: claims,
                expires: DateTime.UtcNow.Add(TimeSpan.FromMinutes(settings.ExpirationInMinutes)),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);

        }
    }
}
