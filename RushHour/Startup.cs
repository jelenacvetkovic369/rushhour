using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using RushHour.Service;
using RushHour.DataAccess;
using RushHour.DataAccess.Repository;
using RushHour.Infrastructure;
using RushHour.Services;
using Microsoft.EntityFrameworkCore;
using RushHour.Extensions;
using AutoMapper;
using RushHour.Service.Mapper;
using Microsoft.AspNetCore.Http;
using RushHour.Service.ServiceInterfaces;

namespace RushHour
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwGen();

            JwtSettings settings = Configuration.GetSection(typeof(JwtSettings).Name).Get<JwtSettings>();
            services.AddAuth(settings);
            services.AddAuthPolicy();

            services.AddAutoMapper(typeof(AutoMapperProfile));
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<ISecurityService, SecurityService>();
            services.AddSingleton(settings);
            services.AddDbContext<ApplicationContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped(typeof(IBaseRepository<,>), typeof(BaseRepository<,>));
            services.AddScoped(typeof(UserRepository));
            services.AddScoped(typeof(RoleRepository));
            services.AddScoped(typeof(PasswordHasher));
            services.AddScoped(typeof(ActivityRepository));
            services.AddScoped(typeof(AppointmentRepository));
            
            services.AddTransient<RoleService>();
            services.AddTransient<UserService>();
            services.AddTransient<RoleService>();
            services.AddTransient<ActivityService>();
            services.AddTransient<AppointmentService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            app.UseHttpsRedirection();

            app.UseMiddleware(typeof(ErrorMiddleware));
            app.UseDeveloperExceptionPage();
            app.UseRouting();
            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });


            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "RushHour v1"));
        }
    }
}
